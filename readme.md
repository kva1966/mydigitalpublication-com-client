# mydigitalpublication.com Web Scraper

## Overview

[mydigitalpublication.com](https://mydigitalpublication.com) is a service used 
by magazine providers such as [Make](https://makezine.com/) and
[ACM Queue](http://queue.acm.org/) to publish digital magazines.

The raison d'être of this tool is to download PDF versions of the magazines 
without using the website. 

**You will need a valid subscription for the publications of interest to use 
this tool**.


## Project Details

This project has 2 parts:

* A Python (3+) module `mydigitalpublication` that has classes to authenticate
against and scrape [mydigitalpublication.com](https://mydigitalpublication.com).

* A utility script, `run.py`, that takes a configuration file, to list and 
download magazine issues using the Python module.


## Module Dependencies

This module has 2 external dependencies:

* [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/).
* [Requests](https://pypi.python.org/pypi/requests)

No packaging of the module is done, and so you will need to manually install
this. Highly recommend the [Anaconda](https://www.continuum.io/why-anaconda) 
(or miniconda) Python distribution for ease of installation and update. Then
run:

        conda install beautifulsoup4 requests

Feel free to contribute a distutils packaging configuration, too. :-)


## Usage

### The `run.py` Utility

`run.py` adds the current working directory to the python path dynamically, so
if you run it from within the checkout directory, it'll have access to the 
module.

        python run.py -h

will give you details. 

`config.ini.sample` is a (hopefully) well-documented sample configuration for
`run.py`.

**Download Behaviour**: Each issue is downloaded to a configured directory.
The script also produces readable filenames for each downloaded issue. If a 
file by the same name exists in the download directory, the download for that
issue is skipped.

### Standalone Module

The `mydigitalpublication` module can be used standalone for your own client 
utilities. As there is no packaging done, you'll have to copy it 
to your python path by some means, and install its dependencies above.


## License

Released under [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0).

