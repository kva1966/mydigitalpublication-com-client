import os
import sys
import pathlib
import argparse

sys.path.append(os.getcwd())

from mydigitalpublication.auth import Login
from mydigitalpublication.issue import IssueList, Issue
from mydigitalpublication.download import Downloader
import mydigitalpublication.util as util

class Runner:
  def __init__(self, configFilePath: str, download: bool):
    self.conf = util.readConfig(configFilePath)
    self.download = download
    self.downloader = Downloader() if download else None

  def run(self):
    for sectionName in self.conf.sections():
      print('=== Processing Publication[%s] ===' % sectionName)
      section = self.conf[sectionName]
      initIssueId = section['initialIssueId']

      # print('%s|%s|%s' % (section['username'], section['password'], section['downloadDirectory']))

      loginSession = Login(initIssueId, section['username'], section['password']).login()
      issues = IssueList(loginSession, initIssueId).list()

      for issue in issues:
        download = True if self.downloader else False
        print('Processing Issue[%s], attempt download? %s' % (issue, download))
        if download:
          self.__downloadIssue(issue, section['downloadDirectory'])
        print('')

      print("=== END Processing Publication[%s] ===\n" % sectionName)

  def __downloadIssue(self, issue: Issue, downloadDir: str):
    pdfUrl = issue.fullPdfUrl
    if pdfUrl:
      outFileName = '%s-%s.pdf' % (util.sanitiseIssueNameForUseAsFilename(issue.name), issue.id)
      print('Downloading [%s - %s - %s] to [%s/%s]' % (issue.id, issue.name,
                                         pdfUrl, downloadDir, outFileName))
      try:
        self.downloader.download(pdfUrl, downloadDir, outFileName)
      except Downloader.OutputFileExistsException as e:
        print("File[%s] exists, not downloading." % pathlib.Path(downloadDir, outFileName))
    else:
      print('Skipping download of %s - %s - No PDF link' % (issue.id, issue.name))


######################

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Find and download issues from mydigitalpublication.com')
  parser.add_argument('-c', '--config', nargs=1, help='config file path', required=True)
  parser.add_argument('-n', '--no-download', action='store_true', help='Lists available only, without downloading')
  args = parser.parse_args()

  try:
    Runner(args.config, False if args.no_download else True).run()
  except KeyboardInterrupt:
    print("User cancelled.")

