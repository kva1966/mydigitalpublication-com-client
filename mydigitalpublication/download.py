import pathlib

import requests

import mydigitalpublication.util as util


class Downloader:
  class OutputFileExistsException(BaseException):
    def __init__(self, msg):
      super(type(self), self).__init__(msg)

  CHUNK_SIZE_BYTES = 4096

  def __init__(self):
    pass

  def download(self, downloadUrl: str, outputDirectory: str, outputFileName: str, failIfOutputFileExists: bool = True) -> None:
    p = pathlib.Path(outputDirectory)
    assert p.exists()
    assert p.is_dir()

    outFilePath = p / outputFileName

    if outFilePath.exists() and failIfOutputFileExists:
      raise Downloader.OutputFileExistsException("[%s] exists, not overwriting." % outFilePath)

    r = requests.get(downloadUrl, stream=True)
    util.assertOk(r)

    with outFilePath.open('wb+') as f:
      for chunk in r.iter_content(Downloader.CHUNK_SIZE_BYTES):
        f.write(chunk)
