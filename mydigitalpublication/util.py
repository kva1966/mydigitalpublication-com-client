import configparser

import requests

MAX_RETRIES = 22

def newRequestsSession() -> requests.Session:
  # http://stackoverflow.com/questions/33895739/python-requests-cant-load-any-url-remote-end-closed-connection-without-respo
  session = requests.Session()
  adapter = requests.adapters.HTTPAdapter(max_retries=MAX_RETRIES)
  session.mount('http://', adapter)
  session.mount('https://', adapter)
  return session

def assertOk(r: requests.Response) -> None:
  assert r.status_code == requests.codes.ok

def readConfig(configFile: str, interpolationInConfig: bool = False) -> configparser.ConfigParser:
  interpolator = configparser.BasicInterpolation() if interpolationInConfig else None
  conf = configparser.ConfigParser(interpolation=interpolator)
  conf.read(configFile)
  return conf

def sanitiseIssueNameForUseAsFilename(issueName: str) -> str:
  return (issueName
          .replace('/', '-')
          .replace(':', ' ')
          .replace(' ', '-')
          .replace('--', '-')
          .replace('.', ''))
