import requests
from bs4 import BeautifulSoup, Tag

import mydigitalpublication.util as util


class Login:
  BASE_URL = 'https://mydigitalpublication.com/publication'
  LOGIN_PAGE_URL_TEMPLATE = BASE_URL + '/login.php?i=%s&sub=1&p=1'
  USERNAME_FORM_KEY = 'reader_login'
  PASSWORD_FORM_KEY = 'reader_password'
  VERIFY_CERTS = True

  
  class LoginException(BaseException):
    def __init__(self, msg):
      super(type(self), self).__init__(msg)

  class MetaUrlParser:
    def __init__(self, content: str):
      self.soup = BeautifulSoup(content, 'html.parser')

    def parse(self) -> str:
      # Search for URL:
      # <meta http-equiv="refresh" content="1;url=<URL>">
      # E.g. <meta http-equiv="refresh" content="1;url=http://www.mydigitalpublication.com/publication/sec_logincheck.php?i=317354&p=2&l=&m=&ver=&view=&pp=&rid=292818105&t=1469100518&pub_id=13033&ah=2158fbe1b7c9f0409644a81e9a8d1a698812bbfd">
      url = None
      for tag in self.soup.find_all('meta'):
        if tag['http-equiv'] == 'refresh':
          content = tag['content']
          url = content[6:]
          assert url.startswith('http'), "Failed to parse tag %s" % tag
          break
      assert url
      return url

  def __init__(self, issueId: str, username: str, password: str):
    self.loginPageUrl = Login.LOGIN_PAGE_URL_TEMPLATE % issueId
    self.username = username
    self.password = password

  def login(self) -> requests.Session:
    loginPage = self.__loadLoginPageContents()
    soup = BeautifulSoup(loginPage, 'html.parser')

    formTag = soup.find('form')
    assert formTag, 'No form found'

    loginUrl = formTag['action']
    assert loginUrl
    loginUrl = loginUrl if loginUrl.lower().startswith('https://') else Login.BASE_URL + '/' + loginUrl

    loginRequestParams = self.__collectInputParams(formTag)
    
    return self.__doLogin(loginUrl, loginRequestParams)

  def __doLogin(self, loginUrl: str, loginRequestParams: dict) -> requests.Session:
    # Login Sequence, GET loginUrl
    # If failed, response will contain:
    # <meta http-equiv="refresh" content="1;url=http://www.mydigitalpublication.com/publication/login.php?i=317354&p=2&msg=y">
    # Essentially the same URL as the login page, with additional params

    # If successful, then we have 2 steps to go:
    # First response:
    # <meta http-equiv="refresh" content="1;url=http://www.mydigitalpublication.com/publication/sec_logincheck.php?i=317354&p=2&l=&m=&ver=&view=&pp=&rid=292818105&t=1469100518&pub_id=13033&ah=2158fbe1b7c9f0409644a81e9a8d1a698812bbfd">
    # By this time we have the login cookie, and have a logged in session.

    # [Optional - this explains getting the publication ID to get the issue list]
    # Following through to that link above, we get the response
    # <meta http-equiv="refresh" content="1;url=/publication/pubbounce.php?i=317354&p=2&l=&m=&ver=&view=&pp=">
    # We also get the login Cookie at this point in time in the response

    # [Optional] Then following that, we get a 302 with the Location
    # Location: /publication?i=317354&p=2&l=&m=&ver=&view=&pp=
    # Optional because, now that we have the cookie, we can ignore pubbounce, and go directly to the desired
    # locations with the login cookie

    ## 1. Login at Login URL, use Session to store cookie information automatically.
    s = util.newRequestsSession()
    r = s.post(loginUrl, data=loginRequestParams, verify=Login.VERIFY_CERTS)
    util.assertOk(r)
    
    ## 2. Inspect content for next URL
    url = Login.MetaUrlParser(r.content).parse()

    if url.find('sec_logincheck.php') > -1:
      # Success, navigate onwards
      s = util.newRequestsSession()
      r = s.get(url, verify=Login.VERIFY_CERTS)
      util.assertOk(r)
    else:
      raise Login.LoginException("Login Failed!")

    return s

  def __loadLoginPageContents(self) -> str:
    r = requests.get(self.loginPageUrl)
    util.assertOk(r)
    return str(r.content, encoding='utf-8')

  def __collectInputParams(self, formTag: Tag) -> dict:
    '''
    <form name="form909" method="post" action="https://mydigitalpublication.com/publication/logincheck.php" onsubmit="return validateForm()">
      <div class="right_w381_h25">
        <div class="right_w156_h25"><p class="text_caption"><input name="reader_login" id="reader_login" type="text" class="textbox" size="100" style="width: 135px; " onclick="/*changecolor()*/"  /></p></div>
        <div class="right_w156_h25"><p class="text_caption"><input name="reader_password" id="reader_password" type="password" class="textbox" size="100" style="width: 135px; " onclick="/*changecolor()*/"  /></p></div>
        <input type="hidden" name="registration" value="0" />
        <input type="hidden" name="pub_id" value="12151" />
        <input type="hidden" name="reader_id" value="" />
        <input type="hidden" name="l" value="" />
        <input type="hidden" name="i" value="297567" />
        <input type="hidden" name="m" value="" />
        <input type="hidden" name="p" value="6" />
        <input type="hidden" name="err" value="https://mydigitalpublication.com/publication/login.php?i=297567&p=6">
        <input type="hidden" name="targ_dom" value="mydigitalpublication.com" />
        <input type="hidden" name="targ_dom_ssl" value="1" />
      </div>
    '''
    params = {}
    for inputTag in formTag.find_all('input'):
      params[inputTag['name']] = inputTag['value'] if 'value' in inputTag.attrs else None

    # At least user name and password 
    assert Login.USERNAME_FORM_KEY in params
    assert Login.PASSWORD_FORM_KEY in params

    params[Login.USERNAME_FORM_KEY] = self.username
    params[Login.PASSWORD_FORM_KEY] = self.password

    return params
