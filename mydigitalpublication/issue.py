import re
from typing import List

import requests
from bs4 import BeautifulSoup

import mydigitalpublication.util as util

class Issue:
  def __init__(self):
    self.id = None
    self.url = None
    self.name = None
    self.description = None
    self.coverPdfUrl = None
    self.coverImgUrl = None
    self.fullPdfUrl = None

  def __repr__(self):
    return 'id[=%s],url=[%s],name=[%s],description=[%s],coverPdfUrl=[%s],coverImgUrl=[%s],fullPdfUrl=[%s]' % (
      self.id,
      self.url,
      self.name,
      self.description,
      self.coverPdfUrl,
      self.coverImgUrl,
      self.fullPdfUrl
    )

class IssueList:
  PUBLICATION_ID_CONTAINING_PAGE_URL_TEMPLATE = 'https://mydigitalpublication.com/publication/?i=%s&p=1&l=&m=&ver=&view=&pp='
  ISSUE_LIST_URL_TEMPLATE = 'https://mydigitalpublication.com/publication/archive.php?id_publication=%s&out=json'
  PDF_LINK_CONTAINING_JSON_URL_TEMPLATE = 'https://mydigitalpublication.com/publication/preloader.php?id_issue=-%s&out=json'

  def __init__(self, loginSession: requests.Session, issueId: str):
    self.loginSession = loginSession
    self.initIssueId = issueId
    self.publicationIdContainingPageUrl = IssueList.PUBLICATION_ID_CONTAINING_PAGE_URL_TEMPLATE % issueId

  def list(self) -> List[Issue]:
    publicationId = self.__obtainPublicationId()
    issueListUrl = IssueList.ISSUE_LIST_URL_TEMPLATE % publicationId

    r = self.loginSession.get(issueListUrl)
    util.assertOk(r)
    json = r.json()

    def buildIssue(issueJson) -> Issue:
      issueAttrs = issueJson['@attributes']
      att = lambda key: issueAttrs[key] if key in issueAttrs else None
      issue = Issue()

      issueId = att('issueid')
      assert issueId, "No issue id!"

      issue.id = att('issueid')
      issue.url = att('url')
      issue.name = att('issue_name')
      issue.description = att('description')
      issue.coverPdfUrl = att('pdf')
      issue.coverImgUrl = att('img')
      issue.fullPdfUrl = self.__obtainFullPdfUrl(issueId)

      return issue

    return [buildIssue(issueJson) for issueJson in json['archive']['issue']]


  def __obtainFullPdfUrl(self, issueId: str) -> str:
    pdfLinkJsonUrl = IssueList.PDF_LINK_CONTAINING_JSON_URL_TEMPLATE % issueId

    r = self.loginSession.get(pdfLinkJsonUrl)
    util.assertOk(r)
    json = r.json()

    toolbarJson = json['preload']['toolbar']['items']['item']
    # print(len(json['preload']['toolbar']['items']['item']))
    # print(json['preload']['toolbar']['items']['item'])

    '''
    # json['preload']['toolbar']['items']['item']

    We see 2 variants of the data lists for an issue. 1 with a downloadable PDF (16 items in list),
    and the other without (14 items in list, some other data missing).

[{'@attributes': {'enabled': 'true', 'link': 'archive', 'jpeg': '/tools/42351/icons/archive.swf', 'description': 'Archive Issues', 'title': 'Archives', 'icon': '0', 'position': 'left', 'id': '2'}}, {'@attributes': {'enabled': 'true', 'link': 'thumbs', 'jpeg': '/tools/42351/icons/thumbs.swf', 'description': 'Thumbnail Pages', 'title': 'Thumbs', 'icon': '0', 'position': 'left', 'id': '3'}}, {'@attributes': {'enabled': 'true', 'link': 'contents', 'jpeg': '/tools/42351/icons/contents.swf', 'description': 'Contents', 'title': 'Contents', 'icon': '0', 'position': 'left', 'id': '4'}}, {'@attributes': {'enabled': 'true', 'link': 'advertisers', 'jpeg': '/tools/42351/icons/advertisers.swf', 'description': 'Advertisers', 'title': 'Advertisers', 'icon': '0', 'position': 'left', 'id': '5'}}, {'@attributes': {'enabled': 'true', 'link': 'zoom', 'jpeg': '/tools/42351/icons/zoom.swf', 'description': 'Zoom', 'icon': '1', 'position': 'center', 'id': '8'}}, {'@attributes': {'enabled': 'true', 'link': 'single', 'jpeg': '/tools/42351/icons/singleview.swf,/tools/42351/icons/spreadview.swf', 'description': 'Single Page View,Spread Pages View', 'icon': '1', 'position': 'center', 'id': '9'}}, {'@attributes': {'enabled': 'true', 'link': 'full', 'jpeg': '/tools/42351/icons/fullscreen.swf', 'description': 'View in Full Screen', 'icon': '1', 'position': 'center', 'id': '11'}}, {'@attributes': {'enabled': 'true', 'link': 'search', 'jpeg': '/tools/42351/icons/search.swf', 'description': 'Search this Publication', 'icon': '1', 'position': 'center', 'id': '10'}}, {'@attributes': {'enabled': 'true', 'link': 'share', 'jpeg': '/tools/42351/icons/share.swf', 'description': 'Share with a Friend', 'title': 'Share', 'icon': '0', 'position': 'right', 'id': '13'}}, {'@attributes': {'enabled': 'false', 'link': 'notes', 'jpeg': '/tools/42351/icons/notes.swf', 'description': 'Add Notes', 'title': 'Notes', 'icon': '0', 'position': 'right', 'id': '22'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/help.swf', 'description': 'Help', 'title': 'Help', 'icon': '0', 'id': '16'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/tools.swf', 'description': 'Tools', 'title': 'Tools', 'icon': '0', 'id': '12'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/settings.swf', 'description': 'Issue Settings', 'title': 'Settings', 'icon': '0', 'id': '15'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/favorites.swf', 'description': 'Add Favorite', 'title': 'Favorite', 'icon': '0', 'id': '23'}}]
Len: 14, No download link

[{'@attributes': {'enabled': 'true', 'link': 'archive', 'jpeg': '/tools/42351/icons/archive.swf', 'description': 'Archive Issues', 'title': 'Archives', 'icon': '0', 'position': 'left', 'id': '2'}}, {'@attributes': {'enabled': 'true', 'link': 'thumbs', 'jpeg': '/tools/42351/icons/thumbs.swf', 'description': 'Thumbnail Pages', 'title': 'Thumbs', 'icon': '0', 'position': 'left', 'id': '3'}}, {'@attributes': {'enabled': 'true', 'link': 'contents', 'jpeg': '/tools/42351/icons/contents.swf', 'description': 'Contents', 'title': 'Contents', 'icon': '0', 'position': 'left', 'id': '4'}}, {'@attributes': {'enabled': 'true', 'link': 'advertisers', 'jpeg': '/tools/42351/icons/advertisers.swf', 'description': 'Advertisers', 'title': 'Advertisers', 'icon': '0', 'position': 'left', 'id': '5'}}, {'@attributes': {'enabled': 'false', 'link': 'print', 'jpeg': '/tools/42351/icons/print.swf', 'description': 'Print Pages', 'title': 'Print', 'icon': '0', 'position': 'left', 'id': '19'}}, {'@attributes': {'enabled': 'true', 'link': 'zoom', 'jpeg': '/tools/42351/icons/zoom.swf', 'description': 'Zoom', 'icon': '1', 'position': 'center', 'id': '8'}}, {'@attributes': {'enabled': 'true', 'link': 'single', 'jpeg': '/tools/42351/icons/singleview.swf,/tools/42351/icons/spreadview.swf', 'description': 'Single Page View,Spread Pages View', 'icon': '1', 'position': 'center', 'id': '9'}}, {'@attributes': {'enabled': 'true', 'link': 'full', 'jpeg': '/tools/42351/icons/fullscreen.swf', 'description': 'View in Full Screen', 'icon': '1', 'position': 'center', 'id': '11'}}, {'@attributes': {'enabled': 'true', 'link': 'search', 'jpeg': '/tools/42351/icons/search.swf', 'description': 'Search this Publication', 'icon': '1', 'position': 'center', 'id': '10'}}, {'@attributes': {'enabled': 'true', 'link': 'share', 'jpeg': '/tools/42351/icons/share.swf', 'description': 'Share with a Friend', 'title': 'Share', 'icon': '0', 'position': 'right', 'id': '13'}}, {'@attributes': {'enabled': 'false', 'link': 'notes', 'jpeg': '/tools/42351/icons/notes.swf', 'description': 'Add Notes', 'title': 'Notes', 'icon': '0', 'position': 'right', 'id': '22'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/help.swf', 'description': 'Help', 'title': 'Help', 'icon': '0', 'id': '16'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/tools.swf', 'description': 'Tools', 'title': 'Tools', 'icon': '0', 'id': '12'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/settings.swf', 'description': 'Issue Settings', 'title': 'Settings', 'icon': '0', 'id': '15'}}, {'@attributes': {'enabled': 'true', 'position': 'right', 'jpeg': '/tools/42351/icons/favorites.swf', 'description': 'Add Favorite', 'title': 'Favorite', 'icon': '0', 'id': '23'}}, {'@attributes': {'enabled': 'true', 'link': 'http://cdn.coverstand.com/38377/301454/b9d3431f9ea21ba8e81fc308218323c5f5340b4e.1.pdf', 'id': '20', 'description': 'Download PDF', 'title': 'PDF'}}]
Len: 16, With download link


    The last piece of data in the list will be the Download link, like so (copied from the 16 len json list above):

{'@attributes': {'enabled': 'true', 'link': 'http://cdn.coverstand.com/38377/301454/b9d3431f9ea21ba8e81fc308218323c5f5340b4e.1.pdf', 'id': '20', 'description': 'Download PDF', 'title': 'PDF'}

    Thus, get last item in list, see if the title is PDF, then get link, otherwise None.
    '''
    lastItem = toolbarJson[len(toolbarJson) - 1]['@attributes']

    if lastItem['title'] == 'PDF':
      assert lastItem['link'], 'No PDF Link'
      return lastItem['link']
    else:
      return None


  def __obtainPublicationId(self) -> str:
    # Add User Agent, otherwise it does not return the expected page
    r = self.loginSession.get(self.publicationIdContainingPageUrl, headers={
      'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:47.0) Gecko/20100101 Firefox/47.0'
    })
    util.assertOk(r)
    soup = BeautifulSoup(r.content, 'html.parser')

    # Publication ID is stored in several spots in the content, but most reliably in
    # a script tag with data containing the text publication_id:
    # 		var options = { "page": 6, "issue_id": 297567, "publication_id": 38068, "timestamp": 201
    # <meta property="og:image" content="http://cdn.coverstand.com/38377/317354/iphonejpg/960/466681a39aefa57e43d828df553064045d318642.jpg"/>
    # The URL in content points to a CDN where the first component of the path is the publication ID

    scriptTags = soup.find_all('script', type='text/javascript')
    regex = re.compile(r'.+?"publication_id": (\d+),.+?', re.DOTALL)

    for tag in scriptTags:
      m = regex.match(tag.get_text())
      if m:
        return m.group(1)

    assert False, 'No publication ID found '
